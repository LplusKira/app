# General rules/ref for SOP
## [SOP Writing rules -- UT prof](http://users.ece.utexas.edu/~bevans/suggested_courses.html#statement)
- less than one page of 11pt
- single-spaced txt
- no context prior to college

## sop revise:
- Galvin
- Fiona
- 侃威
- lindy@eskimo.com
- ?'dear scholar sis'

## Points
1. Introduction.
```
  The 1st sentence: I am applying for an MS ECE degree at The University of Texas at Austin because I would like to be a design engineer.
  For the second sentence, you could say that you're finishing a BSEE degree at university z with specializations in x and y. 
  The third sentence could express how the undergraduate specializations are related to the specialization you'd like to pursue in graduate school.
```
2. __Experiences in undergraduate electives__. Proactive attitudes
3. Experiences in senior design project course and in __undergraduate research__
4. Experiences __working in industry__
5. Summary. Why are you applying to this particular graduate program? __Which research projects__ and/or research centers interest you? Which __faculty__ would you like to work with and why (__give at least three faculty__). __After graduation (career plan)__?
