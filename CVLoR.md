# General rules/ref for CV/LoR

## CV
- [templates](https://www.cmu.edu/career/documents/sample-resumes-cover-letters/sample-resumes_scs.pdf)
- [Tips from CMU](https://www.cmu.edu/career/documents/quick-tips/quick_tips_resume_essentials_2018_final.pdf)

## LoR
- [requirements](https://www.csie.ntu.edu.tw/~hchsiao/policy.html)
- [cmli final project](http://webcache.googleusercontent.com/search?q=cache:uTRO5yn7IO0J:cad-contest.el.cycu.edu.tw/problem_E/default.htm+&cd=2&hl=en&ct=clnk&gl=tw)
