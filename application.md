# Programs
## 15-programs list (sorted by D line):
| Name/D line/AR date | g/G/T/#(17enrollment)/$ (tuition)/year-taken/Location/FinAid/links | memo (traits/murmur) | requirements (loR,Trans,CV,SOP) |
|:-------------|:-------------|:-------------|:-------------|
| [CMU CSD MSCS][1-1]/December,10,2018 | X/Xx/100v/43k(y)/2y/X/? |
||                  [CMU CSD MSCS course handbook][] | 
||                  [CMU CSD MSCS app][] | 
|||| - CV: education, research & publications, work, scholarships awarded, prizes and honors received, society memberships, and extracurricular activities |
|||| - SOP |
|||| - Pub |
|||| - $125 + 75 * 2 (Max: 3 programs) |
||||||||||||||||||||||||||||
| [CMU LTI MSAII][1-2]/December,10,2018 | X/Xx/100v/43k(y)/2y/X/? |
||                  [CMU LTI MSAII app][] | 
||| - identify potential __AI applications__ and __develop and deploy AI solutions__ to __large practical problems__ | - $125 + 75 * N (Max: 3 programs) | 
||| - students, implement __AI systems responsive to market needs__ | - Video Essay | 
||||||||||||||||||||||||||||
| [UCI MCS][30]/December,15,2018/__April 15__ | X/Xx/80v/43k(y)/X/CA/? |
||                  [UCI MCS app][] |
||                  [UCI overview][] |
||||||||||||||||||||||||||||
| [UCI MSCS][30]/December,15,2018/__April 15__ | X/Xx/80v/50k/X/CA/? |  |  |
||                  [UCI MSCS app][] |
||||||||||||||||||||||||||||
| [USC MSCS-SciE][20]/December,15,2018 | X/Xx/90v/36k(y)/X/CA/? |  |  |
||                  [USC MSCS-SciE Contacts][] | 
||                  [USC MSCS-SciE app][] | 
|||| - Supporting Information |
|||| - Case ID: 8141852850 |
||||||||||||||||||||||||||||
| [UCSB MSCS][37]/December,15,2018 | 3.0/Xx/100v/41k(y)/X/CA/? |  |  |
||                  [UCSB MSCS app][] | 
|||| - Statements & Supplemental Documents: Personal Achievements/Contributions Statement |
||||||||||||||||||||||||||||
| [UIUC MCS][5]/December,15,2018/March,15th | 3.2/Xx/Xv/27k(y)/1.5y/Illinois(IL)/? |
||                  [UIUC MCS app][] |
||                  [UIUC MCS research][] |
||| - Required coursework (or commensurate experience) in __object-oriented programming__, | - Residency: proof of funding documentation (at a later time) | 
||| - __data structures__ |
||| - __algorithms__ |
||| - __linear algebra and probability/statistics__ |
||||||||||||||||||||||||||||
| [UCSD CSE][16]/December,17,2018/April,30,2019 | 3.0/X/85/32k(y)/X/CA/? |
||                  [UCSD CSE app][] |
||| - __BS in mathematics is preferred__ |
||||||||||||||||||||||||||||
| [CMU SV MSIT-MOB][1]/December,31,2018 | X/X/X/X/X/CA/? |
||                  [CMU SV MSIT-MOB app][] |
||                  [CMU SV MSIT-MOB fin support][] |
|||| - Academic History |
||||||||||||||||||||||||||||
| [CMU ISR MSE][1]/January,10,2019 | X/X/100/43k(y)/2y(195 __units__)/X/? |
||                  [CMU 87 a overview][] |
||                  [CMU ISR MSE application][] |
||| - __technical leaders__  |
||| - __managing large, diverse teams and complex projects__.|
||||||||||||||||||||||||||||
| [CMU ISR MSIT-SE][1]/January,15,2019 | X/X/100/43k(y)/1.5y(141 __units__)/X/? | 
||                  [CMU ISR MSIT-SE application][] |
||                  [CMU ISR MSIT-SE credits][] | 
||| - __technical leaders__  |
||| - __managing large, diverse teams and complex projects__. | 
||||||||||||||||||||||||||||
| [CMU SV MSSE][1]/January,15,2018/__April,15__ | X/X/X/50k(y)/1y(97 __units__)/X/? |
||                  [CMU SV MSSE app][] |
||| - __software engineering within ECE__ |
||| - __architects and project leaders building systems.__ |
||| - complex __systems challenges__ from the real world. |
||||||||||||||||||||||||||||
| [Rice MCS][20]/January,30,2019 | X/X/90/46k(y)/X/TX/? |
||                  [Rice MCS reason][] |
||                  [Rice MCS app][] |
||||||||||||||||||||||||||||
| [Cornell Meng][6]/February,15,2019 | X/X/22+100/57k(y)/1y/NY/? |
||                  [Cornell Meng app][] |
||| - __foundation in business__ | - Professional Profile: Résumé |
||| - __a strong foundation in engineering__. | - Personal Statement: Computer Science Essay |
||||||||||||||||||||||||||||
| [Columbia software-systems][13]/February,15,2019 | X/X/X/46k(y)/X/NY/? |
||                  [Columbia software-systems D lines][] |
||                  [Columbia software-systems app][] |
||                  [Columbia software-systems reason][] |
||| - __transition--__friendly |
||||||||||||||||||||||||||||
| [UW-Madison CS PMP MS][13]/March,15,2019 | X/145/92/49K ~= 9.7 \* 5/X/WI/X |
||                  [UW-Madison CS PMP MS requirements][] |
||                  [UW-Madison CS PMP MS Process][] |
||                  [UW-Madison CS PMP MS FAQ][] |
||                  [UW-Madison CS PMP MS app][] |
||| - intentional for __working professionals__ (part-time in its design)  | - Supplemental Apps: work experience |
|||| - Supplemental Apps: URLs for additional materials |
|||| - Statements & CV: Statements & CV |
||||||||||||||||||||||||||||


## Waiting list (sorted by D line):
| Name/D line | g/G/T/#(17enrollment)/$ (tuition)/year-taken/Location/FinAid/links | memo (traits/murmur) | requirements loR/G/Trans/CV/(PS=SOP) |
|:-------------|:-------------|:-------------|:-------------|
| [USC MSCS][20]/December,15,2018 | X/X/90/36k(y)/X |
||||||||||||||||||||||||||||
| [USC MSCS-SE][20]/December,15,2018 | X/X/90/36k(y)/X/CA/? |
||| - __software development skills__ |
||| - __leadership__ in software engineering. |
||| - __education for the future__ |
||||||||||||||||||||||||||||
| [UCSC][58]/Jan,10,2019 | 3.0/X/100/57k(y)/X |


## Disadvantages:
- non-CS b.s.


## Filters:
- working 'quota' (companies policy for limiting employee #, admission # (~ students X 2))
- scholar shop
- ranking
- geography (locality for recruiting)
- program specialization
- ($) financial support
- faculty research


## Filtered schools
#### CS
- UW Mich, NEU, UPenn (few admitted Taiwanese records)
- USC CS (CS B.S. only)
- UChicago ('various' students)
- UW-Madison MSCS (PMP instead)
#### CE
- CMU, UIUC, GATech, UTA, Cornell, UoW (no CE)
- Columbia (looks boring)
#### Others
- UPenn MCIT (“完全沒有”電腦科學相關經驗)
- CMU LTI MIIS ("CMU LTI MSAII" seems comparatively [easier](https://www.1point3acres.com/bbs/thread-438154-1-1.html)?) 


## Suggestions:
| David Chou | Liu | Jejing | Austin |
|:-------------|:-------------|:-------------|:-------------|
| - visit scholars in TW (from US) | - USC | - target: usc | northwestern |
| - contact profs | - UCSD | - reach: ucsb | - chigago (track) |
| - internal ref | - CMU SE | | - rougtester |
|              | - 佛吉尼雅 | | stony brook |
|              | - Columbia | | northclarastate |
|              | - NYU | | - uta |
|              | - JHK | | - boston |
|||                         | - asu |
|||                         | - 北卡州大 |


## Ref of positioning:
#### [数字媒体技术+28+26+22+27+153][]
- 很彩票的：UIUC(mcs), UTA, UCB(MEng), Georgia Institute of Technology, CMU（一些中等偏水的项目？）
- 偏彩票的：RICE, UCSD, Purdue
- 主申偏保底（？）的：UCI, TAMU

#### [测控技术与仪器+29+29+24+28+158][]
- 彩票：UCSD, UW, UIUC, CMU
- 主申： Columbia, Duke, Upenn,
- 保底：USC, UCI， NEU

#### [CS+?+?+22+?+155+工作导向][]
- 冲刺：UCSD ,UCSB, RICE
- 主审：NYU-tandon, NEU, USC, UCI, WUSTL, TAMU,OSU
- 保底：ROCHESTER,NCSU, UCSC

#### [CS+?+?+23+?+155][]
- 冲刺：CMU, UCSD, UIUC (MCS, MSCS), Geogea tech (MSCS)
- 主审：USC general，UCI MSCS (vs ?MSC), RICE
- [critic#1](http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=438360&page=1&authorid=241027)

#### [EE+26+25+23+25+160+某科技公司的R&D+10years][]
- AD:
CMU INI MSIT-MOB MS; JHU CS MS; Columbia CE MS; UW-Madison CS PMP MS; Stony Brook CS MS; Rochester CS MS (Tuition Waiver); NYU Tandon CS MS (W/7000 per y); NYU information system MS
- Rejection: 
NEU CS MS; UW-Madison CS MS; Purdue CS MS; Gatech CS MS

- Pending:
TAMU CE MS; UCI CS MS; NWU CE MS; USC CS-Software Engineering MS; Rutgers CS MS

#### [ee+28+27+23+25+153][]
- Dream School：
CMU INI/MSIT-MOB; UIUC MCS; GeorgiaTech MSCS; UMich MSCSE; UW-Madison MSCS 及 PMP
- 主力申請：
Columbia MSCS; UCSD MSCS; UPenn MSCIS; Purdue MSCS; Rice MCS; USC MSCS; NYU Courant MSCS; UC Irvine MCS; TAMU MSCS
- 保底：
UCD MSCS; UCSB MSCS

## Liked:
- [choose schools](https://www.ptt.cc/bbs/studyabroad/M.1524406567.A.614.html)
- [choose school#2](https://www.ptt.cc/bbs/studyabroad/M.1508563586.A.D37.html)
- [misc](https://www.ptt.cc/bbs/studyabroad/M.1445344828.A.33B.html)
- [similar background choices](https://www.ptt.cc/bbs/studyabroad/M.1503626316.A.3F1.html)
- [similar background memo](https://www.ptt.cc/bbs/studyabroad/M.1534341681.A.8FA.html)
- [18 app](https://www.zhihu.com/question/271404778)
- [CMU programs list](https://www.cs.cmu.edu/masters-programs)
- [costs](https://www.1point3acres.com/bbs/thread-292018-1-1.html)
- [Pay the goddamn bill](http://www.sohu.com/a/115324873_115801)


## Rmk
- [ETS dep codes](https://www.ets.org/s/toefl/pdf/dept_code_list.pdf)
- [Upload "official" transcript](https://www.1point3acres.com/bbs/thread-105099-1-1.html)
- major gpa (use all ECON courses; convert them through [this](https://blog.prepscholar.com/gpa-chart-conversion-to-4-0-scale))
```
  Econ only (4.3)
  4.20 = (4.3 * (4 + 4 + 3 + 3 + 4 + 3 + 3 + 4 + 3 + 3) + 4 * (3 + 3 + 2 + 3 + 2) + 3.7 * (2))/((4 + 4 + 3 + 3 + 4 + 3 + 3 + 4 + 3 + 3) + (3 + 3 + 2 + 3 + 2) + 2)

  Econ only (4.0)
  3.99 = (4 * (4 + 4 + 3 + 3 + 3 + 3 + 4 + 3 + 3 + 4 + 2 + 3 + 3 + 3 + 2) + 3.7 * (2))/((4 + 4 + 3 + 3 + 3 + 3 + 4 + 3 + 3 + 4 + 2 + 3 + 3 + 3 + 2) + (2))

  Econ + Math (4.3)
  3.91 = ((4.3 * (4 + 4 + 3 + 3 + 4 + 3 + 3 + 4 + 3 + 3) + 4 * (3 + 3 + 2 + 3 + 2) + 3.7 * (2)) + (4.3 * (3 + 3 + 4 + 4 + 3 + 3) + 4 * (4 + 3) + 3.7 * 3 + 3.3 * (4 + 3 + 3) + 3 * (3 + 3 + 3) + 2.7 * (3 + 3)))/(((4 + 4 + 3 + 3 + 3 + 3 + 4 + 3 + 3 + 4 + 2 + 3 + 3 + 3 + 2) + (2)) + ((3 + 3 + 4 + 4 + 3 + 3) + (4 + 3) + 3 + (4 + 3 + 3) + (3 + 3 + 3) + (3 + 3)))

  Econ + Math (4.0)
  3.76 = ((4.0 * (4 + 4 + 3 + 3 + 4 + 3 + 3 + 4 + 3 + 3) + 4 * (3 + 3 + 2 + 3 + 2) + 3.7 * (2)) + (4.0 * (3 + 3 + 4 + 4 + 3 + 3) + 4 * (4 + 3) + 3.7 * 3 + 3.3 * (4 + 3 + 3) + 3 * (3 + 3 + 3) + 2.7 * (3 + 3)))/(((4 + 4 + 3 + 3 + 3 + 3 + 4 + 3 + 3 + 4 + 2 + 3 + 3 + 3 + 2) + (2)) + ((3 + 3 + 4 + 4 + 3 + 3) + (4 + 3) + 3 + (4 + 3 + 3) + (3 + 3 + 3) + (3 + 3)))
```
- [last minutes](https://www.linstitute.net/archives/95013) for test/sending-scores dates
```
  Generally, send TOEFL scores no later than 11/25 for those with deadlines on 12/10
  Send GRE scores no later than 11/30 for those with deadlines on 12/10

  Each report takes $20/$27 respectively for TOEFL/GRE.
```
- [$](https://1point3acres.com/bbs/thread-292018-1-1.html)
- [CMU how long](http://mse.isri.cmu.edu/software-engineering/web4-distance/FAQ.html)
- [Num of admissions](http://www.1point3acres.com/bbs/thread-136174-1-1.html)
- [scholar shop](http://firekou.pixnet.net/blog/post/23300407-%E5%AD%B8%E5%BA%97%E7%9A%84%E8%BF%B7%E6%80%9D)
- [CMU PA programs](https://www.cs.cmu.edu/masters-programs)
- [CMU SV programs](http://sv.cmu.edu/programs/index.html)


[CMU ISR MSE]: http://mse.isri.cmu.edu/software-engineering/web3-programs/MSE/index.html
[CMU ISR MSIT-SE]: http://mse.isri.cmu.edu/software-engineering/web4-distance/MSIT-SE/index.html
[CMU CSD MSCS]: https://www.csd.cs.cmu.edu/academics/masters/admissions
[CMU SV MSSE]: https://www.ece.cmu.edu/academics/ms-se/index.html
[CMU SV MSIT-MOB]: https://www.cmu.edu/ini/academics/msit/
[USC MSCS]: https://viterbigradadmission.usc.edu/programs/masters/msprograms/computer-science/ms-computer-science/
[USC MSCS-DS]: https://viterbigradadmission.usc.edu/programs/masters/msprograms/computer-science/ms-cs-data-science/
[USC MSCS-SE]: https://viterbigradadmission.usc.edu/programs/masters/msprograms/computer-science/ms-cs-software-engineering/
[USC MSCS-SciE]: https://viterbigradadmission.usc.edu/programs/masters/msprograms/computer-science/ms-cs-scientists-engineers/
[Columbia software-systems]: http://www.cs.columbia.edu/education/ms/softwaresystems/
[Columbia ML]: http://www.cs.columbia.edu/education/ms/machinelearning/
[UCLA MSCS]: https://www.cs.ucla.edu/graduate-admission-frequently-asked-questions/
[UCSD CSE]: https://cse.ucsd.edu/graduate/ucsd-cse-graduate-admissions
[UCSB MSCS]: https://www.cs.ucsb.edu/education/grad/admissions
[UCI MCS]: https://mcs.ics.uci.edu/prospective-students/admissions-requirement/
[UCI MSCS]: https://www.ics.uci.edu/grad/degrees/degree_cs.php
[UCI MSSE]: https://www.informatics.uci.edu/grad/ms-software-engineering/
[GATech MSCS]: https://www.cc.gatech.edu/academics/degree-programs/masters/computer-science/admissionreqs
[NYU-TANDON MSCS]: https://engineering.nyu.edu/academics/programs/computer-science-ms
[Cornell Meng]: https://tech.cornell.edu/admissions/meng-application/
[Rice MCS]: https://csweb.rice.edu/application-graduate-students
[UIUC MCS]: https://cs.illinois.edu/academics/graduate/professional-mcs-program/campus-master-computer-science
[UIUC MCS research]: http://cs.illinois.edu/research/
[TAMU]: https://engineering.tamu.edu/cse/academics/graduate-program/graduate-admissions.html
[SCU]: https://www.scu.edu/engineering/graduate/admissions-requirements/
[SJSU]: http://www.sjsu.edu/graduateadmissions/admission-requirements/test-requirements/
[UTA]: https://www.cs.utexas.edu/graduate/prospective-students/apply
[UW-Madison CS PMP MS]: http://www.cs.wisc.edu/academics/graduate-programs/professional-masters/faq
[UCSC]: https://www.soe.ucsc.edu/departments/computer-science-and-engineering/graduate/degree-requirements-cmps

[CMU 87 a overview]: https://www.cs.cmu.edu/overview-programs
[CMU ISR MSIT-SE application]: https://admissions.scs.cmu.edu/apply/?sr=43e00faa-4494-4477-80ca-7d5fd5eea06a
[CMU ISR MSE application]: https://admissions.scs.cmu.edu/apply/
[CMU CSD MSCS course handbook]: https://www.csd.cs.cmu.edu/sites/default/files/mscs-handbook-2018-2019.pdf
[CMU CSD MSCS application]: https://applygrad.cs.cmu.edu/apply/index.php?domain=1
[CMU ISR MSIT-SE credits]: http://mse.isri.cmu.edu/software-engineering/web4-distance/FAQ.html
[Columbia software-systems D lines]: https://gradengineering.columbia.edu/graduate-admissions/application-requirements
[UCI overview]: https://www.ics.uci.edu/grad/admissions/Prospective_ApplicationProcess.php
[SCU D lines]: https://www.scu.edu/engineering/graduate/admissions-deadlines/
[UTA D lines]: https://login.cs.utexas.edu/sites/default/files/images/Admissions%20Checklist%202019.pdf
[UW-Madison CS PMP MS requirements]: https://grad.wisc.edu/admissions/requirements/
[UW-Madison CS PMP MS Process]: https://www.cs.wisc.edu/academics/graduate-programs/professional-masters/apply
[UW-Madison CS PMP MS FAQ]: http://www.cs.wisc.edu/academics/graduate-programs/professional-masters/faq

[UIUC MCS app]: https://choose.illinois.edu/apply

[Cornell Meng app]: https://cornelltech.secure.force.com/CornellTech/wizardsignon

[GATech MSCS app]: https://www.applyweb.com/forms/gatechg

[UTA app]: https://www.cs.utexas.edu/graduate/prospective-students/apply
[UTA app1]: http://www.applytexas.org/
[UTA app2]: https://apply.cs.utexas.edu/grad/edit_app/instructions.html
[UTA LoR]: https://utexas.app.box.com/s/e392f2d8ia6prvgckqbghirqrhs74c53

[Columbia software-systems app]: https://mice.cs.columbia.edu/recruit/index.php
[Columbia software-systems reason]: https://www.1point3acres.com/bbs/thread-437408-1-1.html

[UCLA MSCS app]: https://apply.grad.ucla.edu/apply/

[UCSD CSE app]: https://apply.grad.ucsd.edu/login

[Rice MCS reason]: https://zhuanlan.zhihu.com/p/45674368

[数字媒体技术+28+26+22+27+153]: http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=443231&extra=page%3D1%26filter%3Dsortid%26sortid%3D157%26searchoption%5B3001%5D%5Bvalue%5D%3D1%26searchoption%5B3001%5D%5Btype%5D%3Dradio%26searchoption%5B3002%5D%5Bvalue%5D%3D1%26searchoption%5B3002%5D%5Btype%5D%3Dradio%26sortid%3D157
[测控技术与仪器+29+29+24+28+158]: http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=444980&extra=page%3D1%26filter%3Dsortid%26sortid%3D157%26searchoption%5B3001%5D%5Bvalue%5D%3D1%26searchoption%5B3001%5D%5Btype%5D%3Dradio%26searchoption%5B3002%5D%5Bvalue%5D%3D1%26searchoption%5B3002%5D%5Btype%5D%3Dradio%26sortid%3D157
[CS+?+?+22+?+155+工作导向]: http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=444242&extra=page%3D1%26filter%3Dsortid%26sortid%3D157%26searchoption%5B3001%5D%5Bvalue%5D%3D1%26searchoption%5B3001%5D%5Btype%5D%3Dradio%26searchoption%5B3002%5D%5Bvalue%5D%3D1%26searchoption%5B3002%5D%5Btype%5D%3Dradio%26sortid%3D157
[CS+?+?+23+?+155]: http://www.1point3acres.com/bbs/thread-438360-1-1.html
[EE+26+25+23+25+160+某科技公司的R&D+10years]: https://www.ptt.cc/bbs/studyabroad/M.1460317929.A.365.html
[ee+28+27+23+25+153]: https://www.ptt.cc/bbs/studyabroad/M.1537281391.A.F03.html

[USC MSCS-SciE Contacts]: https://viterbigradadmission.usc.edu/contact/
[CMU CSD MSCS app]: https://applygrad.cs.cmu.edu/apply/index.php?domain=1
[UCI MCS app]: https://apply.grad.uci.edu/apply/
[UCI MSCS app]: https://apply.grad.uci.edu/apply/
[USC MSCS-SciE app]: https://usc.liaisoncas.com/applicant-ux/#/login
[UCSB MSCS app]: https://www.graddiv.ucsb.edu/eapp/app/Index.aspx

[CMU LTI MSAII]: https://msaii.cs.cmu.edu/
[CMU LTI MSAII app]: https://applygrad.cs.cmu.edu/apply/index.php?domain=1
[CMU LTI MIIS]: https://lti.cs.cmu.edu/intranet/miis

[CMU SV MSIT-MOB app]: https://gradadmissions.engineering.cmu.edu/apply
[CMU SV MSSE app]: https://gradadmissions.engineering.cmu.edu/apply/

[Rice MCS app]: https://gradapply.rice.edu/

[UW-Madison CS PMP MS app]: https://apply.grad.wisc.edu/Account/Login
[CMU SV MSIT-MOB fin support]: https://engineering.cmu.edu/education/graduate-programs/financial-support/index.html
