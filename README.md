# Table of Contents

- [Admissions List](#markdown-header-admissions-list)
- [Pending programs List](#markdown-header-pending-programs-list)
- [Rejections list](#markdown-header-rejections-list)
- [Cost of attendance](#markdown-header-cost-of-attendance)
- [Misc](#markdown-header-misc)

# Admissions list

| Name | $ (tuition)/years/Location/FinAid/links | memo (traits/murmur) |
|:-------------|:-------------|:-------------|
| [CMU SV MSSE][1] | [CMU SV MSSE Cost Of Attendance (65k/y)](#markdown-header-cmu-sv-msse-cost-of-attendance)/1.5y(97 __units__)/CA/ |
||                  [CMU SV MSSE app][] |
||                  [CMU SV Expenditure][] |
||| [CMU SV MSSE Rmk2 2016](http://www.zesteducation.com/2016/zest-activities_0523/478.html) |
||| [CMU SV MSSE Rmk1 2015](https://www.1point3acres.com/bbs/thread-125194-1-1.html) |
||||||||||||||||||||||||||||
| [CMU ISR MSE][1] | [CMU ISR MSE Cost Of Attendance (76k/y)](#markdown-header-CMU-ISR-MSE-Cost-Of-Attendance)/2y(195 __units__)/PA/ |
||                  [CMU ISR MSE application][] |
||||||||||||||||||||||||||||
| [Rice MCS][20] | [Rice MCS Cost Of Attendance (64k/y)](#markdown-header-rice-mcs-cost-of-attendance)/1.5y/TX/ |
||                  [Rice MCS reason][] |
||                  [Rice MCS app][] |
||||||||||||||||||||||||||||
| [UCI MSCS][30] | [UCI MSCS Cost Of Attendance (71k/y)](#markdown-header-uci-cost-of-attendance)/2y/CA/ |
||                  [UCI MSCS app][] |
||||||||||||||||||||||||||||
| [Columbia software-systems][13] | [Columbia software-systems (81k/y)](#markdown-header-columbia-software-systems)/2y/NY/ |
||                  [Columbia software-systems app][] |
||                  [Columbia software-systems reason][] |
||| [Columbia Rmk1 2019](https://www.douban.com/note/710445153/) |


# Pending Programs List

| Name/AR date | $ (tuition)/year-taken/Location/FinAid/links | memo (traits/murmur) |
|:-------------|:-------------|:-------------|
| [CMU SV MSIT-MOB][1]/__April,30,2019__ | X/X/CA/? |
||                  [CMU SV MSIT-MOB app][] |
||                  [CMU SV MSIT-MOB fin support][] |
||| - __wait list__ & do NOT send anything more |
||| - Academic History |
||| If you do not receive an email from us by _April 30_, you will not be offered a seat in the Fall 2019 class. |
||||||||||||||||||||||||||||
| [UCSD CSE][16]/April,30,2019 | 32k(y)/X/CA/? |
||                  [UCSD CSE app][] |
||| - __BS in mathematics is preferred__ |
||||||||||||||||||||||||||||
| [Cornell Meng][6]/1y/NY/? |
||                  [Cornell Meng app][] |
||| - __foundation in business__ |
||| - __a strong foundation in engineering__. |
||||||||||||||||||||||||||||
| [USC MSCS-SciE][20]/??,??,2019 | 36k(y)/X/CA/? |
||                  [USC MSCS-SciE Contacts][] | 
||                  [USC MSCS-SciE app][] | 
||| - Case ID: 8141852850 |
||||||||||||||||||||||||||||
| [UCSB MSCS][37]/April,30,2019 | 41k(y)/X/CA/? |  |  |
||                  [UCSB MSCS app][] | 
||||||||||||||||||||||||||||


# Rejections list

| Name/D line/AR date | $ (tuition)/year-taken/Location/FinAid/links |
|:-------------|:-------------|
| [CMU CSD MSCS][1-1]/December,10,2018 | 43k(y)/2y/X/? |
||||||||||||||||||||||||||||
| [CMU LTI MSAII][1-2]/December,10,2018 | 43k(y)/2y/X/? |
||||||||||||||||||||||||||||
| [UCI MCS][30]/December,15,2018/__April 15__ | 43k(y)/X/CA/? |
||                  [UCI MCS app][] |
||||||||||||||||||||||||||||
| [UIUC MCS][5]/December,15,2018/March,15th | 27k(y ... ?)/1.5y/Illinois(IL)/? |
||||||||||||||||||||||||||||
| [CMU ISR MSIT-SE][1]/January,15,2019 | 43k(y)/1.5y(141 __units__)/X/? | 
||                  [CMU ISR MSIT-SE application][] |
||||||||||||||||||||||||||||


# Cost Of Attendance

## CMU SV MSSE Cost Of Attendance

```
  One academic year:

  V0.0.佛系
  Standard Required Fees = Student Activities Fee + Transportation Fee + Technology Fee
                = 216 + 216 + 420
                = 852
  Health Insurance = 2339
  Room N Food = 9364 + 5620
              = 14984
  Tuition Rates = 47300

  Total = Mandatory Fee + Health Insurance + Room N Food + Tuition Rates
        = 852 + 2339 + 14984 + 47300
        = 65475

  v0.0.official
  Total = Cost of Attendance without Tuition + Tuition Rates
        = 24133 + 47300
        = 71433
  # Ref: https://www.cmu.edu/sfs/tuition/graduate/cit.html
```


## CMU ISR MSE Cost Of Attendance

```
  One academic year:

  V0.0.佛系
  Health Insurance = 2573
  IDK Fee = 872
  Room N Food = 21875
  Books n Supplies = 2212
  Tuition Rates = 24412 * 2
                = 48824

  Total = Health Insurance + IDK Fee + Room N Food + Books n Supplies + Tuition Rates
        = 2573 + 872 + 21875 + 2212 + 48824
        = 76356

  # Ref (in attachment International Student Information Form 2019.pdf): https://mail.google.com/mail/u/0/?hl=en#search/carnegie+mellon/FMfcgxwBVzsNGPcCfqPRHgwHKdBwlfDr
```


## Rice MCS Cost Of Attendance

```
  One academic year:

  V0.0.佛系
  IDK fee = 583
  Health Insurance = 2682
  Room and Board = 14140 (?)
  Tuition Rates = 46200

  Total = Health Insurance + IDK fee + Housing fee + Tuition Rates
        = 583 + 2682 + 14140 + 46200
        = 63605

  # Ref: https://financialaid.rice.edu/cost-attendance
  # Ref2: https://gradadmissions.rice.edu/apply/update?id=548c2343-ab57-4d55-93d3-2e5385de88fc
```


## UCI MSCS Cost Of Attendance

```
  One academic year:

  V0.0.佛系
  (on campus) Living Cost = 38286
  Tuition Rates = 32567

  Total = (on campus) Living Cost + Tuition Rates
        = 38286 + 32567
        = 70853

  # Ref: http://catalogue.uci.edu/informationforprospectivestudents/expensestuitionandfees/#EstimatedExpenses
  # - Reduced-Fee Part-Time Study Program
```


## Columbia software-systems Cost Of Attendance

```
  One academic year:

  V0.0.佛系
  Tuition Rates = 48000
  Others (ROOM n BOARD included) = 33061

  Total = Tuition Rates + Others (ROOM n BOARD included)
        = 48000 + 33061
        = 81061

  # Ref: https://www.quora.com/What-is-the-total-cost-of-doing-an-MS-at-Columbia-University-including-living-expenses
```

# Misc

## Liked
- [misc](https://www.ptt.cc/bbs/studyabroad/M.1445344828.A.33B.html)
- [similar background choices](https://www.ptt.cc/bbs/studyabroad/M.1503626316.A.3F1.html)
- [similar background memo](https://www.ptt.cc/bbs/studyabroad/M.1534341681.A.8FA.html)
- [18 app](https://www.zhihu.com/question/271404778)
- [CMU programs list](https://www.cs.cmu.edu/masters-programs)
- [costs](https://www.1point3acres.com/bbs/thread-292018-1-1.html)
- [Pay the goddamn bill](http://www.sohu.com/a/115324873_115801)

## Rmk

Info regarding [$](https://1point3acres.com/bbs/thread-292018-1-1.html), [Num of admissions](http://www.1point3acres.com/bbs/thread-136174-1-1.html),
and [scholar shop (!?)](http://firekou.pixnet.net/blog/post/23300407-%E5%AD%B8%E5%BA%97%E7%9A%84%E8%BF%B7%E6%80%9D)

Major GPA (use all ECON courses; convert them through [this](https://blog.prepscholar.com/gpa-chart-conversion-to-4-0-scale))

```
  Econ only (4.3)
  4.20 = (4.3 * (4 + 4 + 3 + 3 + 4 + 3 + 3 + 4 + 3 + 3) + 4 * (3 + 3 + 2 + 3 + 2) + 3.7 * (2))/((4 + 4 + 3 + 3 + 4 + 3 + 3 + 4 + 3 + 3) + (3 + 3 + 2 + 3 + 2) + 2)

  Econ only (4.0)
  3.99 = (4 * (4 + 4 + 3 + 3 + 3 + 3 + 4 + 3 + 3 + 4 + 2 + 3 + 3 + 3 + 2) + 3.7 * (2))/((4 + 4 + 3 + 3 + 3 + 3 + 4 + 3 + 3 + 4 + 2 + 3 + 3 + 3 + 2) + (2))

  Econ + Math (4.3)
  3.91 = ((4.3 * (4 + 4 + 3 + 3 + 4 + 3 + 3 + 4 + 3 + 3) + 4 * (3 + 3 + 2 + 3 + 2) + 3.7 * (2)) + (4.3 * (3 + 3 + 4 + 4 + 3 + 3) + 4 * (4 + 3) + 3.7 * 3 + 3.3 * (4 + 3 + 3) + 3 * (3 + 3 + 3) + 2.7 * (3 + 3)))/(((4 + 4 + 3 + 3 + 3 + 3 + 4 + 3 + 3 + 4 + 2 + 3 + 3 + 3 + 2) + (2)) + ((3 + 3 + 4 + 4 + 3 + 3) + (4 + 3) + 3 + (4 + 3 + 3) + (3 + 3 + 3) + (3 + 3)))

  Econ + Math (4.0)
  3.76 = ((4.0 * (4 + 4 + 3 + 3 + 4 + 3 + 3 + 4 + 3 + 3) + 4 * (3 + 3 + 2 + 3 + 2) + 3.7 * (2)) + (4.0 * (3 + 3 + 4 + 4 + 3 + 3) + 4 * (4 + 3) + 3.7 * 3 + 3.3 * (4 + 3 + 3) + 3 * (3 + 3 + 3) + 2.7 * (3 + 3)))/(((4 + 4 + 3 + 3 + 3 + 3 + 4 + 3 + 3 + 4 + 2 + 3 + 3 + 3 + 2) + (2)) + ((3 + 3 + 4 + 4 + 3 + 3) + (4 + 3) + 3 + (4 + 3 + 3) + (3 + 3 + 3) + (3 + 3)))
```

## Docs

- SOP.pdf -- general version/template for SOP
- CV.pdf -- general version/template for CV
- other markdown files -- for corresponding TODOs & remarks
- Other files/dirs -- deprecated

## Filters
- working 'quota' (companies policy for limiting employee #, admission # (~ students X 2))
- scholar shop
- ranking
- geography (locality for recruiting)
- program specialization
- ($) financial support
- faculty research


[CMU ISR MSE]: http://mse.isri.cmu.edu/software-engineering/web3-programs/MSE/index.html
[CMU ISR MSIT-SE]: http://mse.isri.cmu.edu/software-engineering/web4-distance/MSIT-SE/index.html
[CMU CSD MSCS]: https://www.csd.cs.cmu.edu/academics/masters/admissions
[CMU SV MSSE]: https://www.ece.cmu.edu/academics/ms-se/index.html
[CMU SV MSIT-MOB]: https://www.cmu.edu/ini/academics/msit/
[USC MSCS]: https://viterbigradadmission.usc.edu/programs/masters/msprograms/computer-science/ms-computer-science/
[USC MSCS-DS]: https://viterbigradadmission.usc.edu/programs/masters/msprograms/computer-science/ms-cs-data-science/
[USC MSCS-SE]: https://viterbigradadmission.usc.edu/programs/masters/msprograms/computer-science/ms-cs-software-engineering/
[USC MSCS-SciE]: https://viterbigradadmission.usc.edu/programs/masters/msprograms/computer-science/ms-cs-scientists-engineers/
[Columbia software-systems]: http://www.cs.columbia.edu/education/ms/softwaresystems/
[Columbia ML]: http://www.cs.columbia.edu/education/ms/machinelearning/
[UCLA MSCS]: https://www.cs.ucla.edu/graduate-admission-frequently-asked-questions/
[UCSD CSE]: https://cse.ucsd.edu/graduate/ucsd-cse-graduate-admissions
[UCSB MSCS]: https://www.cs.ucsb.edu/education/grad/admissions
[UCI MCS]: https://mcs.ics.uci.edu/prospective-students/admissions-requirement/
[UCI MSCS]: https://www.ics.uci.edu/grad/degrees/degree_cs.php
[UCI MSSE]: https://www.informatics.uci.edu/grad/ms-software-engineering/
[GATech MSCS]: https://www.cc.gatech.edu/academics/degree-programs/masters/computer-science/admissionreqs
[NYU-TANDON MSCS]: https://engineering.nyu.edu/academics/programs/computer-science-ms
[Cornell Meng]: https://tech.cornell.edu/admissions/meng-application/
[Rice MCS]: https://csweb.rice.edu/application-graduate-students
[UIUC MCS]: https://cs.illinois.edu/academics/graduate/professional-mcs-program/campus-master-computer-science
[UIUC MCS research]: http://cs.illinois.edu/research/
[TAMU]: https://engineering.tamu.edu/cse/academics/graduate-program/graduate-admissions.html
[SCU]: https://www.scu.edu/engineering/graduate/admissions-requirements/
[SJSU]: http://www.sjsu.edu/graduateadmissions/admission-requirements/test-requirements/
[UTA]: https://www.cs.utexas.edu/graduate/prospective-students/apply
[UW-Madison CS PMP MS]: https://www.cs.wisc.edu/graduate/professional-masters-program-2/
[UCSC]: https://www.soe.ucsc.edu/departments/computer-science-and-engineering/graduate/degree-requirements-cmps

[CMU 87 a overview]: https://www.cs.cmu.edu/overview-programs
[CMU ISR MSIT-SE application]: https://admissions.scs.cmu.edu/apply/?sr=43e00faa-4494-4477-80ca-7d5fd5eea06a
[CMU ISR MSE application]: https://admissions.scs.cmu.edu/apply/
[CMU CSD MSCS course handbook]: https://www.csd.cs.cmu.edu/sites/default/files/mscs-handbook-2018-2019.pdf
[CMU CSD MSCS application]: https://applygrad.cs.cmu.edu/apply/index.php?domain=1
[CMU ISR MSIT-SE credits]: http://mse.isri.cmu.edu/software-engineering/web4-distance/FAQ.html
[Columbia software-systems D lines]: https://gradengineering.columbia.edu/graduate-admissions/application-requirements
[UCI overview]: https://www.ics.uci.edu/grad/admissions/Prospective_ApplicationProcess.php
[SCU D lines]: https://www.scu.edu/engineering/graduate/admissions-deadlines/
[UTA D lines]: https://login.cs.utexas.edu/sites/default/files/images/Admissions%20Checklist%202019.pdf
[UW-Madison CS PMP MS requirements]: https://grad.wisc.edu/admissions/requirements/
[UW-Madison CS PMP MS Process]: https://www.cs.wisc.edu/academics/graduate-programs/professional-masters/apply
[UW-Madison CS PMP MS FAQ]: http://www.cs.wisc.edu/academics/graduate-programs/professional-masters/faq

[UIUC MCS app]: https://choose.illinois.edu/apply

[Cornell Meng app]: https://cornelltech.secure.force.com/CornellTech/wizardsignon

[GATech MSCS app]: https://www.applyweb.com/forms/gatechg

[UTA app]: https://www.cs.utexas.edu/graduate/prospective-students/apply
[UTA app1]: http://www.applytexas.org/
[UTA app2]: https://apply.cs.utexas.edu/grad/edit_app/instructions.html
[UTA LoR]: https://utexas.app.box.com/s/e392f2d8ia6prvgckqbghirqrhs74c53

[Columbia software-systems app]: https://mice.cs.columbia.edu/recruit/index.php
[Columbia software-systems reason]: https://www.1point3acres.com/bbs/thread-437408-1-1.html

[UCLA MSCS app]: https://apply.grad.ucla.edu/apply/

[UCSD CSE app]: https://apply.grad.ucsd.edu/login

[Rice MCS reason]: https://zhuanlan.zhihu.com/p/45674368

[数字媒体技术+28+26+22+27+153]: http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=443231&extra=page%3D1%26filter%3Dsortid%26sortid%3D157%26searchoption%5B3001%5D%5Bvalue%5D%3D1%26searchoption%5B3001%5D%5Btype%5D%3Dradio%26searchoption%5B3002%5D%5Bvalue%5D%3D1%26searchoption%5B3002%5D%5Btype%5D%3Dradio%26sortid%3D157
[测控技术与仪器+29+29+24+28+158]: http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=444980&extra=page%3D1%26filter%3Dsortid%26sortid%3D157%26searchoption%5B3001%5D%5Bvalue%5D%3D1%26searchoption%5B3001%5D%5Btype%5D%3Dradio%26searchoption%5B3002%5D%5Bvalue%5D%3D1%26searchoption%5B3002%5D%5Btype%5D%3Dradio%26sortid%3D157
[CS+?+?+22+?+155+工作导向]: http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=444242&extra=page%3D1%26filter%3Dsortid%26sortid%3D157%26searchoption%5B3001%5D%5Bvalue%5D%3D1%26searchoption%5B3001%5D%5Btype%5D%3Dradio%26searchoption%5B3002%5D%5Bvalue%5D%3D1%26searchoption%5B3002%5D%5Btype%5D%3Dradio%26sortid%3D157
[CS+?+?+23+?+155]: http://www.1point3acres.com/bbs/thread-438360-1-1.html
[EE+26+25+23+25+160+某科技公司的R&D+10years]: https://www.ptt.cc/bbs/studyabroad/M.1460317929.A.365.html
[ee+28+27+23+25+153]: https://www.ptt.cc/bbs/studyabroad/M.1537281391.A.F03.html

[USC MSCS-SciE Contacts]: https://viterbigradadmission.usc.edu/contact/
[CMU CSD MSCS app]: https://applygrad.cs.cmu.edu/apply/index.php?domain=1
[UCI MCS app]: https://apply.grad.uci.edu/apply/
[UCI MSCS app]: https://apply.grad.uci.edu/apply/
[USC MSCS-SciE app]: https://usc.liaisoncas.com/applicant-ux/#/login
[UCSB MSCS app]: https://www.graddiv.ucsb.edu/eapp/app/Index.aspx

[CMU LTI MSAII]: https://msaii.cs.cmu.edu/
[CMU LTI MSAII app]: https://applygrad.cs.cmu.edu/apply/index.php?domain=1
[CMU LTI MIIS]: https://lti.cs.cmu.edu/intranet/miis

[CMU SV MSIT-MOB app]: https://gradadmissions.engineering.cmu.edu/apply
[CMU SV MSSE app]: https://gradadmissions.engineering.cmu.edu/apply/

[Rice MCS app]: https://gradapply.rice.edu/

[UW-Madison CS PMP MS app]: https://apply.grad.wisc.edu/Account/Login
[CMU SV MSIT-MOB fin support]: https://engineering.cmu.edu/education/graduate-programs/financial-support/index.html

[CMU SV Expenditure]: https://www.cmu.edu/sfs/tuition/graduate/cit.html
